<?php

/**
 * Public template
 *
 * @link       http://wordpress.org/plugins/rate-my-post/
 * @since      2.0.0
 *
 * @package    Rate_My_Post
 * @subpackage Rate_My_Post/public/partials
 */
?>

<?php
  // variables
  $post_id = ( $post_id ) ? $post_id : get_the_id();
  $rmp_options = get_option( 'rmp_options' );
  $rmp_custom_strings = $this->custom_strings( $post_id );
  $rating_icon_type = self::icon_type();
  $avg_rating = Rate_My_Post_Common::get_average_rating( $post_id );
  $vote_count = Rate_My_Post_Common::get_vote_count( $post_id );
  $icon_classes = self::icons_classes( $post_id, true );
  $results_text = $this->rating_widget_results_text( $rmp_options, $avg_rating, $vote_count, $post_id );
  $max_rating = Rate_My_Post_Common::max_rating();
  $custom_class = $this->custom_class( $post_id );

?>

<!-- Rate my Post Plugin -->
<div
  class="rmp-widgets-container rmp-wp-plugin rmp-main-container js-rmp-widgets-container js-rmp-widgets-container--<?php echo $post_id; ?><?php echo $custom_class; ?>"
  data-post-id="<?php echo $post_id; ?>"
>
  <?php do_action( 'rmp_before_all_widgets' ); ?>
  <!-- Rating widget -->
  <div class="rmp-rating-widget js-rmp-rating-widget">

    <?php if( str_replace( ' ', '', $rmp_custom_strings['rateTitle'] ) ): ?>
      <p class="rmp-heading rmp-heading--title">
        <?php echo $rmp_custom_strings['rateTitle']; ?>
      </p>
    <?php endif; ?>

    <?php if( str_replace( ' ', '', $rmp_custom_strings['rateSubtitle'] ) ): ?>
      <p class="rmp-heading rmp-heading--subtitle">
        <?php echo $rmp_custom_strings['rateSubtitle']; ?>
      </p>
    <?php endif; ?>

    <div class="votes">
        <div>
            <span class="vote-counts"><?php echo $vote_count; ?></span>
            <span>VOTE(S)</span>
        </div>
    </div>

    <div class="rmp-rating-widget__icons">
      <ul class="rmp-rating-widget__icons-list js-rmp-rating-icons-list">
          <li class="rmp-rating-widget__icons-list__icon js-rmp-rating-item btn-vote" data-descriptive-rating="Not at all useful" data-value="1">            
            <!--i class="js-rmp-rating-icon rmp-icon rmp-icon--ratings rmp-icon--star"></i -->Vote  
          </li>
      </ul>
      <!--button class="rmp-rating-widget__icons-list__icon js-rmp-rating-item confirm-vote" data-descriptive-rating="Not at all useful" data-value="1">
        
      </button -->
      <!--button class="confirm-vote js-rmp-rating-item" data-value="1">Confirm Vote</button -->
    </div>


    <p class="rmp-rating-widget__hover-text js-rmp-hover-text"></p>

    <button class="rmp-rating-widget__submit-btn rmp-btn js-submit-rating-btn">
      <?php echo $rmp_custom_strings['submitButtonText']; ?>
    </button>

    <p class="rmp-rating-widget__msg js-rmp-msg"></p>

  </div>

  <!--Structured data -->
  <?php echo $this->structured_data( $post_id, $vote_count ); ?>

  <?php// if ( $rmp_options['social'] === 2 ): ?>
    <!-- Social widget -->
    <?php echo $this->social_widget( $post_id ); ?>
  <?php // endif; ?>

  <?php if ( $rmp_options['feedback'] === 2 ): ?>
    <!-- Feedback widget -->
    <?php echo $this->feedback_widget( $post_id ); ?>
  <?php endif; ?>
  <?php do_action( 'rmp_after_all_widgets' ); ?>
</div>

<script type="text/javascript">
 /*
  jQuery(function($) {
    
    if(rmp_frontend.loggedIn.length == 0) {
      console.log('NOT LOGGED');
      $('.rmp-rating-widget .rmp-heading--subtitle').text('Please log-in to vote.');
      $('.btn-vote').remove();
    }

    if( $('.js-rmp-msg').text() == 'Thank you for voting this video!' ) {
      $('.btn-vote').remove();
    }

  });
  */
</script>