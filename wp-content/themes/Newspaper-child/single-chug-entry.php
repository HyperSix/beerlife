<?php /* Template Name: Chugh Entries */ ?>
<?php session_start(); get_header(); ?>
<style type="text/css">
    .vote-container ul{
        margin: 0;
        padding: 0;
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        margin-bottom: 20px !important;    
    }
    .vote-container ul li{
        flex: 0 0 50%;
        list-style: none;
        margin-left: 0;
    }
    .vote-container ul li img{
        width: 100%;
        height: auto;
    }
    .vote-container ul li .ig-name{
        font-size: 30px;
        text-align: center;
        font-family: 'Karla', sans-serif;
    }
    .vote-container ul li .ig-name i{
        color: #b7142b;
    }
    .vote-container ul li .votes{
        width: 100%;
        text-align: center;
        margin-bottom: 15px;
    }
    .vote-container ul li .votes span{
        color: #b7142b;
        font-weight: 700;
    }
    .vote-container ul li .votes .vote-counts{
        font-size: 80px;
        line-height: 80px;
        display: block;
        margin-bottom: -5px;
    }
    .vote-container {
        padding: 10px;
    }
    .btn-vote {
        margin: auto;
        background-color: #B7142B;
        padding: 10px 20px;
        color: #fff;
        border-radius: 10px;
        cursor: pointer;
    }
    .confirm-vote.disabled {
        background-color: #cccccc;
        cursor: auto;
    }
    .video-title {
        margin-bottom: 40px;
        margin-top: -50px;
        color: #b7142b;
    }
    .video_content {
        position: relative;
        width: auto;
        height: 100%;
        max-width: 100%;
        max-height: 472px;
        margin: auto;
        display: block;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        -ms-border-radius: 4px;
        -o-border-radius: 4px;
        border-radius: 4px;
    }
    .vote-details {
        padding: 5px;
    }
    .rmp-widgets-container.rmp-wp-plugin.rmp-main-container .rmp-rating-widget__icons-list__icon {
        margin: auto;
        font-size: 20px;
    }
    .rmp-heading--subtitle {
        font-size: 18px;
    }
    .rmp-widgets-container.rmp-wp-plugin.rmp-main-container .rmp-rating-widget__submit-btn {
        display: none !important;
    }
    /*
    .rmp-rating-widget__icons-list {
        display: none !important;
    }
    */
    .rmp-widgets-container.rmp-wp-plugin.rmp-main-container .rmp-social-widget {
        display: block;
        position: relative;
        bottom: -50px;    
    }
    .rmp-social-widget .rmp-icon--social {
        padding: 10px;
        font-size: 18px;
        width: 40px;
    }
    .rmp-rating-widget--has-rated .btn-vote {
        display: none !important;
    }
</style>
<section class="header-slider">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <div class="content-box">
                        <h1 class="header-title">Beerlife Chug Contest</h1>
                        <span class="header-subtitle">The chug with the most votes will win a <em>cash prize</em>.</span>
                    </div>
                </div>
            </div>
        </div>
</section>
<nav>
    <ul class="bl-nav-list">
        <li><a href="/entries" class="bl-nav-link">Instagram Video Entries</a></li>
        <li><a href="/leaderboard" class="bl-nav-link"><i class="fas fa-crown"></i> Leaderboards</a></li>
         <li><a href="/past-champion" class="bl-nav-link">Past Champions</a></li>
        <li>
            <a href="#">
                <i class="fas fa-caret-down"></i>
            </a>
        </li>
    </ul>
        <ul class="bl-control-list">
            <?php if ( is_user_logged_in() )  { ?>
                <li id="loggedIn"><a href="/wp-login.php?action=logout" class="bl-nav-link logout" style="cursor: pointer;">Logout</a></li>
            <?php }else{ ?>
                <li id="loggedOut"><a href="/wp-login.php" class="bl-nav-link login" style="cursor: pointer;">Login</a></li>
            <?php } ?>
            <li><a href="" class="bl-nav-link submit">Submit a Video</a></li>
        </ul>
</nav>
 <!-- div class="filter-controls">
    <form id='search-form' class="search-filter">
        <div class="search-box">
            <input type="text" class="search-bar" placeholder="SEARCH FOR CHUG VIDEO ENTRIES" />
            <div class='search-btn'>
                <button type='submit'>
                    <i class="fa fa-search search"></i>
                </button>
            </div>
        </div>
    </form>
     <form action="" class="date-filter">
        <div class="select-box">
            <span>Chug Contest:</span>
            <select name="date" id="date">
                <option value="May">May 2020</option>
                <option value="June">June 2020</option>
                <option value="July">July 2020</option>
                <option value="August">August 2020</option>
            </select>
        </div>
    </form>
</div  --> 
    <section class="video-entries">

    <div class="container vote-container">
                <ul>
                    <li>
                        <video class="video_content" src="<?php echo get_the_content(); ?>" controls="" style="opacity: 1; z-index: 1;"></video>
                    </li>

                    <?php $pid = get_the_ID(); ?>

                    <li id="sbi_<?php echo $pid; ?>">
                        <div class="text-center vote-details">
                            <h5 class="video-title"><?php echo get_the_excerpt(); ?></h5>
                             <?php echo do_shortcode("[ratemypost id='$pid']"); ?>
                        </div>
                    </li>
                </ul>
               
                <!-- modal close button -->
                <a  class="modal__close demo-close vote-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
    </div>

    </section>
    <section class="faq-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/faq-icon.jpg" class="faq-icon" alt="Icon" />
                    <h2 class="section-title">Frequently asked question</h2>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Is there a limit to how many times I can participate?</h3>
                    <p class="faq-answer">Nope. There no limits how many times you can submit a new chug or drink. Simply drink, tag our account, and submit. We will donate $1 per submission.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Where exactly will the money be donated?</h3>
                    <p class="faq-answer">The money will be donated to DirectRelief.ord/emergency/coronavirus-outbrek/. This fund is dedicated to giving protective gear and critical care medications to as many health workers as possbile. Direct relief of the largest administrative charities in the space.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Why did we choose First Responders?</h3>
                    <p class="faq-answer">First Responders are currently fighting on the front lines in the battle against COVID-19. Central to the mission of Beerlife is the have fun and give back. We at Beerlife wanted to find a fun way to do good for those most in need during these challenging times.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">What is Beerlife Official</h3>
                    <p class="faq-answer">Beerlife is an irreverent entertainment and lifestyle brand for those who love beer. Our goal is to entertain our audience and make their lives more fun. We do that through our various social media pages on Instagram, Facebook, Pinterest, Beerlife.com, and original content.</p>
                </div>
            </div>
        </div>
    </section>
    <div id="login-modal">
        <div class="modal-body">
            <div class="modal-content">
                <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/beerlife-logo.png" class="modal-logo" alt="Site Logo"/>
                <h2 class="modal-title">Login to Beerlife Chug Contest</h2>
                <br>
                <span id="error_output" style="color: red; font-size: 13px; font-weight: 500;"></span>
                <form id="login_form">
                    <input type="text" placeholder="Email Address" name="email" class="modal-input">
                    <input type="password" placeholder="Password" name="password" class="modal-input">
                </form>
                
                <a href="#" class="forgot-password">Forgot Password?</a>
                <br>
                <button type="button" class="modal-btn btn-login" id="loginBtn">Login Now</button>
                <span class="modal-separator">OR</span>
                <button type="button" class="modal-btn btn-login-facebook"><i class="fab fa-facebook"></i> Continue With Facebook</button>
                <button type="button" class="modal-btn btn-login-google"><i class="fab fa-google"></i> Continue With Google</button>
                <span class="terms-agreement">By continuing, you agree to BeerLife’s<em> Terms of Service, Privacy Policy</em></span>

                <a class="sign-up">Not yet registered? Sign up</a>
                <!-- modal close button -->
                <a class="modal__close demo-close login-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>

    <div id="register-modal">
        <div class="modal-body">
            <div class="modal-content">
                <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/beerlife-logo.png" class="modal-logo" alt="Site Logo"/>
                <h2 class="modal-title">Create an Account</h2>
                <br>
                <form id="register_form">
                    <input type="text" placeholder="Email Address" name="r_email" id="r_email" class="modal-input">
                    <input type="password" placeholder="Password" name="r_password" id="r_password" class="modal-input">
                    <input type="number" placeholder="Age" name="age" class="modal-input">
                </form>
                <a href="#" class="forgot-password">Forgot Password?</a>
                <br>
                <button type="button" class="modal-btn btn-login" id="registerBtn">Sign Up Now</button>
                <span class="modal-separator">OR</span>
                <button type="button" class="modal-btn btn-login-facebook"><i class="fab fa-facebook"></i> Continue With Facebook</button>
                <button type="button" class="modal-btn btn-login-google"><i class="fab fa-google"></i> Continue With Google</button>
                <span class="terms-agreement">By continuing, you agree to BeerLife’s<em> Terms of Service, Privacy Policy</em></span>

                <a href="#" class="sign-up">Already a Member? Login</a>
                <!-- modal close button -->
                <a class="modal__close demo-close register-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>

    <div id="vote-modal" class="vote-modal">
        <div class="modal-body">
            <div class="modal-content">
                <iframe class="vote-entry" src="" scrolling="no"></iframe>
                <span class="loading"><i class="fas fa-3x fa-spinner fa-spin"></i></span>
                <!-- modal close button -->
                <a  class="modal__close demo-close vote-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>
    <div id="video-modal" class="video-modal">
        <div class="modal-body">
            <div class="modal-content">
                <div class="sbi_lb-container-wrapper">
                   <div class="sbi_lb-container" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                      <video class="sbi_video" src="" controls=""  style="opacity: 1; z-index: 1;"></video>
                   </div>
                </div>
                <!-- modal close button -->
                <a  class="modal__close demo-close video-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>

<script type="text/javascript">

 jQuery(function($){
    $('.btn-vote').click(function(){
        console.log('vote!');

        jQuery(this).html('<i class="fas fa-spinner fa-spin"></i>')
        setTimeout(function(){ jQuery('.js-submit-rating-btn').click(); }, 1000);

        $('body').on('DOMSubtreeModified', ".js-rmp-msg", function(el) {         
                $('.btn-vote').remove(); 
                    setTimeout(function(){ 
                        if( $('.rmp-rating-widget .rmp-rating-widget__msg--alert').length > 0 ) {
                            console.log('DONT COUNT!!!!!');
                            return false;
                        }else{
                        	var newvotes =  parseInt($('.vote-counts').text()) + 1;
                            $('.vote-counts').text(newvotes);              
                        }

                    }, 1500);
            ///location.reload();
        });  

    });  
 });
    
</script>
<?php get_footer(); ?>
