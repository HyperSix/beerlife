    <!-- VIDEO -->
    <div id="video-modal" class="video-modal">
        <div class="modal-body">
            <div class="modal-content">
                <div class="sbi_lb-container-wrapper">
                   <div class="sbi_lb-container" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                      <video class="sbi_video" src="" controls=""  style="opacity: 1; z-index: 1;"></video>
                   </div>
                </div>
                <!-- modal close button -->
                <a  class="modal__close demo-close video-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>

    <!-- VOTE -->
    <div id="vote-modal" class="vote-modal">
        <div class="modal-body">
            <div class="modal-content">
                <iframe class="vote-entry" src="" scrolling="no"></iframe>
                <span class="loading"><i class="fas fa-3x fa-spinner fa-spin"></i></span>
                <!-- modal close button -->
                <a  class="modal__close demo-close vote-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>    