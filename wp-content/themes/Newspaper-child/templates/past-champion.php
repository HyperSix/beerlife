<?php /* Template Name: Past Champions */ ?>
<?php 

    $args = array(
        'post_type'=> 'chug-entry',
        'showposts' => 12,
        'ignore_sticky_posts' => true,
        'meta_key'      => 'winner',
        'meta_value'    => '1'        
    );
    query_posts($args);

?>
<?php get_header(); ?>
<style type="text/css">
        .ig-name {
            margin-top: 0;
            color: #333;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            height: 30px;    
            width: 270px;                    
        }
        .rmp-social-widget .rmp-icon--social {
            padding: 10px;
            font-size: 18px;
            width: 40px;
        }   
        #video-modal {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            display: block;
            background: rgba(0,0,0,0.9);
            height: -webkit-fill-available;
            display: none;
            z-index: 9999;
            background: rgba(0,0,0,0.9);
            /* -webkit-box-align: start; */
            align-items: center;
            -webkit-box-pack: center;
        }
        #video-modal .modal-body {
            display: flex;
            position: relative;
            max-width: 480px;
            width: 100%;
            margin: auto;
            background: #ffffff;
        }
        #video-modal .modal-content {
            width: 100%;
            display: block;
            margin: auto;
            position: relative;
            background-color: #fff;
        }
        #video-modal .sbi_video {
            display: block;
            position: relative;
        }
        #video-modal .modal__dialog {
          max-width: 1000px;
            width: 980px;
          padding: 1.2rem;
            margin-top: 150px;
        }
        .media-pop {
            cursor: pointer;
        }      
        .bl-row{
            justify-content: center;
        }
</style>
<section class="header-slider">
    <div class="bl-container td-stretch-content">
        <div class="bl-row">
            <div class="bl-col-md-12 text-center">
                <div class="content-box">
                    <h1 class="header-title">Beerlife Chug Contest</h1>
                    <span class="header-subtitle">The chug with the most votes will win a <em>cash prize</em>.</span>
                </div>
            </div>
        </div>
    </div>
</section>
<nav>
    <ul class="bl-nav-list">
        <li><a href="/entries" class="bl-nav-link">Instagram Video Entries</a></li>
        <li><a href="/leaderboard" class="bl-nav-link"><i class="fas fa-crown"></i> Leaderboards</a></li>
         <li><a href="/past-champion" class="bl-nav-link">Past Champions</a></li>
        <li>
            <a href="#">
                <i class="fas fa-caret-down"></i>
            </a>
        </li>
    </ul>
    <ul class="bl-control-list">
        <?php if ( is_user_logged_in() )  { ?>
            <li id="loggedIn"><a href="/wp-login.php?action=logout" class="bl-nav-link logout" style="cursor: pointer;">Logout</a></li>
        <?php }else{ ?>
            <li id="loggedOut"><a href="/wp-login.php" class="bl-nav-link login" style="cursor: pointer;">Login</a></li>
        <?php } ?>
        <li><a href="" class="bl-nav-link submit">Submit a Video</a></li>
    </ul>
</nav>
     <!--div class="filter-controls">
        <form id='search-form' class="search-filter">
            <div class="search-box">
                <input type="text" class="search-bar" placeholder="SEARCH FOR CHUG VIDEO ENTRIES" />
                <div class='search-btn'>
                    <button type='submit'>
                        <i class="fa fa-search search"></i>
                    </button>
                </div>
            </div>
        </form>
         <form action="" class="date-filter">
            <div class="select-box">
                <span>Year: </span>
                <select name="date" id="date">
                    <option value="May">2020</option>
                    <option value="June">2019</option>
                    <option value="July">2018</option>
                    <option value="August">2017</option>
                </select>
            </div>
        </form>
    </div --> 
        <section class="past-champions">
            <div class="bl-container td-stretch-content">
                <div class="bl-row">
                    <div class="bl-col-md-12"><h1 class="content-title">Previous Chug Champions</h1></div>

                    <?php            
                    if (have_posts()) {
                        while ( have_posts() ) : the_post(); 
                            $pid     = get_the_ID();
                            $title   = get_post_meta( $pid, 'winning_title', true );
                            $won     = get_post_meta( $pid, 'amount_won', true );
                            $donated = get_post_meta( $pid, 'amount_donated', true );
                            $featured_img_url = get_the_post_thumbnail_url($pid,'full'); 
                            $content = get_the_content();
                            $desc = get_the_excerpt();
                            $votes =  get_post_meta( $pid, 'total_votes', true );
                        ?>                    
                        <div class="bl-col-md-6">
                            <div class="content-box text-center">
                                <ul class="top-video">
                                    <li>
                                        <div class="top-content">
                                            <div>
                                            <a class="sbi_photo media-pop" data-url="<?php echo $content; ?>"  rel="nofollow">
                                                <img src="<?php echo esc_url($featured_img_url); ?>" class="video-img"  />
                                            </a>
                                            </div>
                                            <div class="votes">
                                                <div>
                                                    <span class="vote-counts"><?php echo $votes; ?></span>
                                                    <span>VOTES</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="ribbon-box"><?php echo $title; ?></div>
                                        <h3 class="ig-name"><?php echo $desc; ?></h3>
                                        <div class="price-won-box">
                                            <span>won</span>
                                            <span class="price"><?php echo number_format($won); ?></span>
                                        </div>
                                        <div class="price-donated-box">
                                            <span>donated</span>
                                            <span class="price"><?php echo number_format($donated); ?></span>
                                        </div>
                                        <br>
                                        <div class="entry-social-share">
                                            <?php echo do_shortcode("[ratemypost-result id='$pid' share='true']"); ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                     <?php endwhile; 
                         // Reset Query
                         wp_reset_query();
                        } ?>
                </div>
            </div>
        </section>
        <section class="faq-section">
            <div class="bl-container td-stretch-content">
                <div class="bl-row">
                    <div class="bl-col-md-12 text-center">
                        <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/faq-icon.jpg" class="faq-icon" alt="Icon" />
                        <h2 class="section-title">Frequently asked question</h2>
                    </div>
                    <div class="bl-col-md-6">
                        <h3 class="faq-question">Is there a limit to how many times I can participate?</h3>
                        <p class="faq-answer">Nope. There no limits how many times you can submit a new chug or drink. Simply drink, tag our account, and submit. We will donate $1 per submission.</p>
                    </div>
                    <div class="bl-col-md-6">
                        <h3 class="faq-question">Where exactly will the money be donated?</h3>
                        <p class="faq-answer">The money will be donated to DirectRelief.ord/emergency/coronavirus-outbrek/. This fund is dedicated to giving protective gear and critical care medications to as many health workers as possbile. Direct relief of the largest administrative charities in the space.</p>
                    </div>
                    <div class="bl-col-md-6">
                        <h3 class="faq-question">Why did we choose First Responders?</h3>
                        <p class="faq-answer">First Responders are currently fighting on the front lines in the battle against COVID-19. Central to the mission of Beerlife is the have fun and give back. We at Beerlife wanted to find a fun way to do good for those most in need during these challenging times.</p>
                    </div>
                    <div class="bl-col-md-6">
                        <h3 class="faq-question">What is Beerlife Official</h3>
                        <p class="faq-answer">Beerlife is an irreverent entertainment and lifestyle brand for those who love beer. Our goal is to entertain our audience and make their lives more fun. We do that through our various social media pages on Instagram, Facebook, Pinterest, Beerlife.com, and original content.</p>
                    </div>
                </div>
            </div>
        </section>

        <div id="video-modal" class="video-modal">
            <div class="modal-body">
                <div class="modal-content">
                    <div class="sbi_lb-container-wrapper">
                       <div class="sbi_lb-container" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                          <video class="sbi_video" src="" controls=""  style="opacity: 1; z-index: 1;"></video>
                       </div>
                    </div>
                    <!-- modal close button -->
                    <a  class="modal__close demo-close video-modal-close">
                        <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                    </a>
                </div>
            </div>
        </div>    

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>        

<?php get_footer(); ?>

<script>
jQuery(function($) {

    var ulLi = jQuery('nav ul.bl-nav-list > li'),
      fa = jQuery('nav ul.bl-nav-list > li:last-of-type a .fa');

    jQuery('nav ul.bl-nav-list').append('<ol></ol>');

    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/entries' class='bl-nav-link'>Instagram Video Entries</a></li>");
    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/leaderboard' class='bl-nav-link'><i class='fas fa-crown'></i> Leaderboards</a></li>");
    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/past-champion' class='bl-nav-link'>Past Champions</a></li>");

    jQuery('nav ul.bl-nav-list > li:last-of-type').on('click', function() {
        fa.toggleClass('fa-bars');
        fa.toggleClass('fa-times');
        jQuery(this).parent().children('ol').slideToggle(500);
    });

    $('.media-pop').click(function(){
        console.log('CLIECKED');
        var url = $(this).data('url');
        $('#video-modal .sbi_video').attr('src', url);
        $('#video-modal').fadeOut(500);
        $('#video-modal').css('display', 'flex').fadeIn(500);
    });


    $('.video-modal-close').click(function(){
        $('#video-modal').fadeOut(500);
    });

});
</script>