<?php
/*
 * Template Name: Single Entry
 * Template Post Type: post, page, product
 */
    session_start(); get_header();

    function getUserIpAddr(){
        $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    $insta_id = $_GET['vid_id'];
    
     $rootPath = $_SERVER['DOCUMENT_ROOT'];
    include_once($rootPath . '/page-share_db.php');


   ?>
<section class="header-slider">
    <div class="bl-container td-stretch-content">
        <div class="bl-row">
            <div class="bl-col-md-12 text-center">
                <div class="content-box">
                    <h1 class="header-title">Beerlife Chug Contest</h1>
                    <span class="header-subtitle">The chug with the most votes will win a <em>cash prize</em>.</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="main-content single-entry-section">
    <div class="bl-container td-stretch-content">
        <div class="bl-row">
            <div class="bl-col-md-12 text-center">
                <div class="content-box">
                    <!-- <h1 class="content-title">Top 10 Beerlife Chug Videos</h1> -->
                    <ul class="top-video">
                        <li>
                            <video src="<?php echo $vid_media; ?>" controls autoplay></video>
                        </li>
                        <li>
                            <!-- <h3 class="ig-name"><i class="fab fa-instagram"></i> bonechugsnharmony</h3> -->
                            <span class="vote-counts"><?php if(!$vote_count){ echo 0;}else{echo $vote_count;} ?></span>
                            <span class="votes">VOTES</span>
                            <br>
                            <!-- <button type="button" data-modal="#vote-modal" class="vote-btn modal__trigger">Vote For This Chug </button> -->
                            <button type="button" class="vote-btn">Vote For This Chug  </button>
                            <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/social-media-icons.png" class="social-media-icons" alt="Social Media Icons"/>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script type="text/javascript">

    $('.vote-btn').click(function(){
        $.ajax({
          url: '../dbconnect.php',
          data: 'instagram_id=' + '<?php echo "sbi_".$insta_id; ?>' + '&type=vote' + '&ip_address=' + '<?php echo getUserIpAddr(); ?>',
          method: 'POST',
          dataType: 'html',
          success: function(data){
            if (data != 1) {
                alert(data);
            }else{
                alert('Vote Successful!');
                location.reload();
            }
            $('#vote-modal').fadeOut(500);
          }
        });
    });

</script>
<?php get_footer();  ?>  