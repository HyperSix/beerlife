jQuery(function() {
    
    $('.vote-btn').click(function(){
        SBI_ITEM = $(this).data('pid');
        var parent = '#sbi_' + $(this).data('pid'); 
        SBI_COUNT = parseInt($(parent).find('.js-rmp-vote-count').text());

        var current = $('#vote-modal .vote-entry').attr('src'),
            url = 'http://localhost/chug-entry/?pid=' + $(this).data('pid');

        if(current == url) {
           $('#vote-modal').css('display', 'flex').fadeIn(500);
           $('#vote-modal .loading').hide();
           $('#vote-modal iframe').show();            
        }else{
            $('#vote-modal .vote-entry').attr('src', url);
             $('#vote-modal').css('display', 'flex').fadeIn(500);
             $('#vote-modal .loading').show();
             $('#vote-modal iframe').hide();

            setTimeout(function(){ 
                $('#vote-modal iframe').show();
               $('#vote-modal .loading').hide();
            }, 2000);            
        }
       
    });


    $('.vote-modal-close').click(function(){
       // SBI_ITEM = $(this).data('pid');
        var parent = '#sbi_' + SBI_ITEM; 
        $(parent).find('.js-rmp-vote-count').text(SBI_COUNT);        
        $('#vote-modal').fadeOut(500);
    });

    /*
    $('.sbi_playbtn, .media-pop').click(function(){
        $parent = $(this).closest('.sbi_item');
        $parent.find('.vote-btn').click();
    });
    */
    
    $('.sbi_playbtn').click(function(){
        $parent = $(this).closest('.sbi_photo_wrap');
        $parent.find('.media-pop').click();
    });

    $('.media-pop').click(function(){
        var url = $(this).data('url');
        $('#video-modal .sbi_video').attr('src', url);
        $('#video-modal').fadeOut(500);
        $('#video-modal').css('display', 'flex').fadeIn(500);
    });


    $('.video-modal-close').click(function(){
        $('#video-modal').fadeOut(500);
    });

});