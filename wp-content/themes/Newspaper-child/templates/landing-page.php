<?php /* Template Name: Landing */ ?>
<?php get_header(); ?>
    <section class="slider-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-6 text-center">
                    <div class="slider-content">
                        <h4>Welcome To</h4>
                        <h1>The BeerLife <br> World Chug<br> Championships</h1>
                        <h2>For Charity</h2>
                        <!--<img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/funds-raised.png" class="funds" alt="Funds" />-->
                        <div class="progress-bg">
                            <div class="progress-bar">
                                <h3 class="raised">$6,345 of $10,000 Raised </h3>
                            </div>
                        </div>
                        <br>
                        <a href="#participate-section" class="participate-btn smooth-scroll">Participate Now</a>
                    </div>
                </div>
                <div class="bl-col-md-6">
                    <img src="https://beerlife.com/wp-content/uploads/2020/08/slider-thumbnail.png" class="slider-thumbnail" alt="Slider Thumbnail" />
                </div>
            </div>
        </div>
    </section>
    <section class="idea-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-6 text-center order-2">
                    <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/ideas-gallery.png" class="ideas-gallery" alt="Idea Gallery" />
                </div>
                <div class="bl-col-md-6 order-1 ">
                    <h2 class="section-title">The Idea and Mission</h2>
                    <p class="paragraph-white">BeerLife is all about having fun and giving back! That's why we launched The BeerLife World Chug Championships to give the best weekend chuggers a forum to have fun, compete, and help raise money for their favorite charities!</p>
                </div>
            </div>
        </div>
    </section>
    <section class="htp-section" id="participate-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <h2 class="section-title text-center">How To Participate</h2>
                </div>
                <div class="bl-col-md-3 text-center">
                    <div class="content-box">
                        <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/record-icon.png" class="participate-icon" alt="Icon" />
                        <h4>1</h4>
                        <p class="details">Record yourself taking a drink or chugging a beer</p>
                    </div>
                </div>
                <div class="bl-col-md-3 text-center">
                    <div class="content-box">
                        <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/instagram.png" class="participate-icon" alt="Icon" />
                        <h4>2</h4>
                        <p class="details">Post it on your instagram feed and tag <a href="https://www.instagram.com/beerlifeofficial/">@beerlifeofficial</a></p>
                    </div>
                </div>
                <div class="bl-col-md-3 text-center">
                    <div class="content-box">
                        <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/beer-icon.png" class="participate-icon" alt="Icon" />
                        <h4>3</h4>
                        <p class="details">Go to <a href="http://beerlife.com/entries">https://beerlife.com/entries</a> and vote for your chug and share it with friends to support your chug as the winner.</p>
                    </div>
                </div>
                <div class="bl-col-md-3 text-center">
                    <div class="content-box">
                        <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/dollar-icon.png" class="participate-icon" alt="Icon" />
                        <h4>4</h4>
                        <p class="details">The chug with the most votes wins a cash prize for themselves AND their favorite Charity!</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cta-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-6">
                    <h2 class="section-title">Beer Chug Contest</h2>
                    <p class="section-description">The chug with the most votes will win a cash <br>prize for themselves and their favorite charity!</p>
                    <br>
                    <a href="#participate-section" class="participate-btn smooth-scroll">Participate Now</a>
                    <a href="<?php echo site_url('entries'); ?>" class="entries-btn">See The Entries</a>
                </div>
            </div>
        </div>
    </section>
    <section class="faq-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/faq-icon.jpg" class="faq-icon" alt="Icon" />
                    <h2 class="section-title">Frequently asked question</h2>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Is there a limit to how many times I can participate?</h3>
                    <p class="faq-answer">Nope. There no limits how many times you can submit a new chug or drink. Simply drink, tag our account, and submit. We will donate $1 per submission.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Where exactly will the money be donated?</h3>
                    <p class="faq-answer">The money will be donated to DirectRelief.ord/emergency/coronavirus-outbrek/. This fund is dedicated to giving protective gear and critical care medications to as many health workers as possbile. Direct relief of the largest administrative charities in the space.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Why did we choose First Responders?</h3>
                    <p class="faq-answer">First Responders are currently fighting on the front lines in the battle against COVID-19. Central to the mission of Beerlife is the have fun and give back. We at Beerlife wanted to find a fun way to do good for those most in need during these challenging times.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">What is Beerlife Official</h3>
                    <p class="faq-answer">Beerlife is an irreverent entertainment and lifestyle brand for those who love beer. Our goal is to entertain our audience and make their lives more fun. We do that through our various social media pages on Instagram, Facebook, Pinterest, Beerlife.com, and original content.</p>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>