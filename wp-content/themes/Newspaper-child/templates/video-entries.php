<?php /* Template Name: Video Entries */ ?>
<?php session_start(); get_header(); ?>
<?php 

    function getUserIpAddr(){
        $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    foreach($_POST as $key => $value){
        $_SESSION[$key] = $value;
    }

    $a  = filter_var_array($_SESSION, FILTER_SANITIZE_STRING);

?>
<section class="header-slider">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <div class="content-box">
                        <h1 class="header-title">Beerlife Chug Contest</h1>
                        <span class="header-subtitle">The chug with the most votes will win a <em>cash prize</em>.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <nav>
        <ul class="bl-nav-list">
            <li><a href="https://beerlife.com/entries" class="bl-nav-link">Instagram Video Entries</a></li>
            <li><a href="https://beerlife.com/leaderboard" class="bl-nav-link"><i class="fas fa-crown"></i> Leaderboards</a></li>
             <li><a href="https://beerlife.com/past-champion" class="bl-nav-link">Past Champions</a></li> 
            <li>
                <a href="#">
                    <i class="fas fa-caret-down"></i>
                </a>
            </li>
        </ul>
        <ul class="bl-control-list">
            <?php if ($_SESSION['status'] == true) { ?>
                <li id="loggedIn"><a data-modal="#login-modal" class="bl-nav-link logout" style="cursor: pointer;">Logout</a></li>
            <?php }else{ ?>
                <li id="loggedOut"><a data-modal="#login-modal" class="bl-nav-link login" style="cursor: pointer;">Login</a></li>
            <?php } ?>
            <li><a href="https://beerlife.com/submit-video/" class="bl-nav-link submit">Submit a Video</a></li>
        </ul>
    </nav>
     <div class="filter-controls">
        <form id='search-form' class="search-filter">
            <div class="search-box">
                <input type="text" class="search-bar" placeholder="SEARCH FOR CHUG VIDEO ENTRIES" />
                <div class='search-btn'>
                    <button type='submit'>
                        <i class="fa fa-search search"></i>
                    </button>
                </div>
            </div>
        </form>
         <form action="" class="date-filter">
            <div class="select-box">
                <span>Chug Contest:</span>
                <select name="date" id="date">
                    <option value="May">May 2020</option>
                    <option value="June">June 2020</option>
                    <option value="July">July 2020</option>
                    <option value="August">August 2020</option>
                </select>
            </div>
        </form>
    </div> 
    <section class="video-entries">
            <?php echo do_shortcode('[instagram-feed type="tagged"  tagged="beerlifeofficial" customtemplate="true" num="12"]');  ?>
    </section>
    <section class="faq-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/faq-icon.jpg" class="faq-icon" alt="Icon" />
                    <h2 class="section-title">Frequently asked question</h2>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Is there a limit to how many times I can participate?</h3>
                    <p class="faq-answer">Nope. There no limits how many times you can submit a new chug or drink. Simply drink, tag our account, and submit. We will donate $1 per submission.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Where exactly will the money be donated?</h3>
                    <p class="faq-answer">The money will be donated to DirectRelief.ord/emergency/coronavirus-outbrek/. This fund is dedicated to giving protective gear and critical care medications to as many health workers as possbile. Direct relief of the largest administrative charities in the space.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Why did we choose First Responders?</h3>
                    <p class="faq-answer">First Responders are currently fighting on the front lines in the battle against COVID-19. Central to the mission of Beerlife is the have fun and give back. We at Beerlife wanted to find a fun way to do good for those most in need during these challenging times.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">What is Beerlife Official</h3>
                    <p class="faq-answer">Beerlife is an irreverent entertainment and lifestyle brand for those who love beer. Our goal is to entertain our audience and make their lives more fun. We do that through our various social media pages on Instagram, Facebook, Pinterest, Beerlife.com, and original content.</p>
                </div>
            </div>
        </div>
    </section>
    <div id="login-modal">
        <div class="modal-body">
            <div class="modal-content">
                <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/beerlife-logo.png" class="modal-logo" alt="Site Logo"/>
                <h2 class="modal-title">Login to Beerlife Chug Contest</h2>
                <br>
                <span id="error_output" style="color: red; font-size: 13px; font-weight: 500;"></span>
                <form id="login_form">
                    <input type="text" placeholder="Email Address" name="email" class="modal-input">
                    <input type="password" placeholder="Password" name="password" class="modal-input">
                </form>
                
                <a href="#" class="forgot-password">Forgot Password?</a>
                <br>
                <button type="button" class="modal-btn btn-login" id="loginBtn">Login Now</button>
                <span class="modal-separator">OR</span>
                <button type="button" class="modal-btn btn-login-facebook"><i class="fab fa-facebook"></i> Continue With Facebook</button>
                <button type="button" class="modal-btn btn-login-google"><i class="fab fa-google"></i> Continue With Google</button>
                <span class="terms-agreement">By continuing, you agree to BeerLife’s<em> Terms of Service, Privacy Policy</em></span>

                <a class="sign-up">Not yet registered? Sign up</a>
                <!-- modal close button -->
                <a class="modal__close demo-close login-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>
    <div id="vote-modal" class="vote-modal">
        <div class="modal-body">
            <div class="modal-content">
                <ul>
                    <li><img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/video-entries-1.jpg" class="video-img" alt="Top Video"/></li>
                    <li>
                        <!-- <h3 class="ig-name"><i class="fab fa-instagram"></i> bonechugsnharmony</h3> -->
                        <div class="votes">
                            <div>
                                <span class="vote-counts">1,241</span>
                                <span>VOTES</span>
                            </div>
                        </div>
                        <form action="" method="POST">
                            <div class="g-recaptcha brochure__form__captcha" id="g-recaptcha"></div>
                            <!-- data-sitekey="6LdQj74ZAAAAAFHVM3WtWbjR47re7p8YirmDuvdw" -->
                            <br/>
                        </form>
                        <div class="text-center">
                            <button class="vote_close_btn modal__close">Confirm Vote</button>
                        </div>
                    </li>
                </ul>
                <!-- modal close button -->
                <a  class="modal__close demo-close vote-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>
    <div id="register-modal">
        <div class="modal-body">
            <div class="modal-content">
                <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/beerlife-logo.png" class="modal-logo" alt="Site Logo"/>
                <h2 class="modal-title">Create an Account</h2>
                <br>
                <form id="register_form">
                    <input type="text" placeholder="Email Address" name="r_email" id="r_email" class="modal-input">
                    <input type="password" placeholder="Password" name="r_password" id="r_password" class="modal-input">
                    <input type="number" placeholder="Age" name="age" class="modal-input">
                </form>
                <a href="#" class="forgot-password">Forgot Password?</a>
                <br>
                <button type="button" class="modal-btn btn-login" id="registerBtn">Sign Up Now</button>
                <span class="modal-separator">OR</span>
                <button type="button" class="modal-btn btn-login-facebook"><i class="fab fa-facebook"></i> Continue With Facebook</button>
                <button type="button" class="modal-btn btn-login-google"><i class="fab fa-google"></i> Continue With Google</button>
                <span class="terms-agreement">By continuing, you agree to BeerLife’s<em> Terms of Service, Privacy Policy</em></span>

                <a href="#" class="sign-up">Already a Member? Login</a>
                <!-- modal close button -->
                <a class="modal__close demo-close register-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>
    <input type="hidden" name="ip_address" id="ip_address" value="<?php echo getUserIpAddr(); ?>">
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<style type="text/css">
    
        .vote_close_btn {
            display: none;
            margin: auto;
            background-color: #B7142B;
            padding: 10px 20px;
            color: #fff;
            border-radius: 10px;
        }

        #login-modal {
            display: none;
        }
        #register-modal {
            display: none;
        }

</style>

<!-- login -->
<script type="text/javascript">

    $(document).ready(function(){
        fillVote();
    });
  
    function fillVote(){
       $.ajax({
          url: '../dbconnect.php',
          data: '&type=assign_vote',
          method: 'POST',
          dataType: 'json',
          success: function(data){
            for (var i = 0; i <= data.length; i++) {
                var id_search = "sbi_"+data[i]['instagram_id'];
                var voteSpan = $('#'+id_search+' .vote-counts');
                $('#sbi_images').find(voteSpan).html(data[i]['vote_counts']);
            }            
          }
        }); 
    }

    $('.video-entries').resize(function(){
        fillVote();
    });

  $('#loginBtn').click(function(){
    $.ajax({
      url: '../dbconnect.php',
      data: $('#login_form').serialize() + '&type=login',
      method: 'POST',
      dataType: 'html',
      success: function(data){
        if (data != 'success') {
            $('#error_output').html(data);
            $('#login_form input').val('');
        }else{
            location.reload();
        }
      }
    });
  });

  $('.logout').click(function(){
    $.ajax({
      url: '../dbconnect.php',
      data: 'type=logout',
      method: 'POST',
      dataType: 'html',
      success: function(data){
        location.reload();
      }
    });
  });

  $('#registerBtn').click(function(){
    $.ajax({
      url: '../dbconnect.php',
      data: $('#register_form').serialize() + '&type=sign',
      method: 'POST',
      dataType: 'html',
      success: function(data){
        location.reload();
        $('#login-modal').fadeOut(500);
      }
    });
  });

</script>

<!-- voting -->
<script type="text/javascript">

    var Cid;
    var voting_voteC;
    var Vid;

    function voteModal(Cid){

        var inst_id = $('#'+Cid+' .sbi_photo img').attr('src');

        voting_voteC = $('#'+Cid+' .vote-counts').html();

        $('#vote-modal .vote-counts').html(voting_voteC);
        $('#vote-modal .video-img').attr('src', inst_id);
    }

    var $element = $(".video-entries");
    var lastHeight = $(".video-entries").css('height');

    checkForChanges();
    function checkForChanges()
    {
        if ($element.css('height') != lastHeight)
        {
            $('.vote-btn').click(function(){
                $('#vote-modal').css('display', 'flex').fadeIn(500);
                Cid = $(this).parent().parent().attr('id');
                $('.vote_close_btn').hide();
                grecaptcha.reset();
                voteModal(Cid);
            });
            lastHeight = $element.css('height'); 
        }

        setTimeout(checkForChanges, 500);
    }
    
    $(document).ready(function(){
        $('.vote-btn').click(function(){
            $('#vote-modal').css('display', 'flex').fadeIn(500);
            Cid = $(this).parent().parent().attr('id');
            $('.vote_close_btn').hide();
            grecaptcha.reset();
            voteModal(Cid);
        });

        $('.share-btn').click(function(){
            Vid = $(this).parent().parent().attr('id');
            Vid = Vid.replace('sbi_', '')
            window.location.replace("https://beerlife.com/single-entry?vid_id="+Vid);
        });
    });

    $('.vote-modal-close').click(function(){
        $('#vote-modal').fadeOut(500);
    });

    $('.login').click(function(){
        $('#login-modal').css('display', 'flex').fadeIn(500);
    });

    $('.login-modal-close').click(function(){
        $('#login-modal').fadeOut(500);
    });
    
    $('.sign-up').click(function(){
        $('#login-modal').fadeOut(500);
        $('#register-modal').css('display', 'flex').fadeIn(500);
    });

    $('.register-modal-close').click(function(){
        $('#register-modal').fadeOut(500);
    });

    var captchaResponse = function(resp) {
        $('.vote_close_btn').show();        
    }

    $('.vote_close_btn').click(function(){
        $.ajax({
          url: '../dbconnect.php',
          data: 'instagram_id=' + Cid + '&type=vote' + '&ip_address=' + $('#ip_address').val(),
          method: 'POST',
          dataType: 'html',
          success: function(data){
            if (data != 1) {
                alert(data);
            }else{
                fillVote();
                alert('Successful Vote!');
            }
            $('#vote-modal').fadeOut(500);
          }
        });
    });

    var onloadCallback = function() {
      var voteCaptcha = grecaptcha.render('g-recaptcha', {
          'sitekey' : '6LdQj74ZAAAAAFHVM3WtWbjR47re7p8YirmDuvdw',
          'callback': captchaResponse
        });
      };
    
</script>


<?php get_footer(); ?>

<script>
jQuery(function() {
  var ulLi = jQuery('nav ul.bl-nav-list > li'),
      fa = jQuery('nav ul.bl-nav-list > li:last-of-type a .fa');
  
   jQuery('nav ul.bl-nav-list').append('<ol></ol>');
  
   jQuery('nav ul.bl-nav-list > ol').append("<li><a href='' class='bl-nav-link'>Instagram Video Entries</a></li>");
   jQuery('nav ul.bl-nav-list > ol').append("<li><a href='' class='bl-nav-link'><i class='fas fa-crown'></i> Leaderboards</a></li>");
   jQuery('nav ul.bl-nav-list > ol').append("<li><a href='' class='bl-nav-link'>Past Champions</a></li>");

  jQuery('nav ul.bl-nav-list > li:last-of-type').on('click', function() {
    fa.toggleClass('fa-bars');
    fa.toggleClass('fa-times');
    jQuery(this).parent().children('ol').slideToggle(500);
  });
});
</script>