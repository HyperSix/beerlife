<?php /* Template Name: Chugh Entries */ ?>
<?php session_start(); get_header(); ?>
<?php 
    $args = array(
        'post_type'=> 'chug-entry',
        'showposts' => 12,
        'ignore_sticky_posts' => true
    );
    query_posts($args);

?>
<script type="text/javascript">
    var SBI_ITEM = '';
    var SBI_COUNT = 1;
</script>
<style type="text/css">
    
        .vote_close_btn {
            display: none;
            margin: auto;
            background-color: #B7142B;
            padding: 10px 20px;
            color: #fff;
            border-radius: 10px;
        }

        #login-modal {
            display: none;
        }
        #register-modal {
            display: none;
        }

        #video-modal {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            display: block;
            background: rgba(0,0,0,0.9);
            height: -webkit-fill-available;
            display: none;
            z-index: 9999;
            background: rgba(0,0,0,0.9);
            /* -webkit-box-align: start; */
            align-items: center;
            -webkit-box-pack: center;
        }
        #video-modal .modal-body {
            display: flex;
            position: relative;
            max-width: 480px;
            width: 100%;
            margin: auto;
            background: #ffffff;
        }
        #video-modal .modal-content {
            width: 100%;
            display: block;
            margin: auto;
            position: relative;
            background-color: #fff;
        }
        #video-modal .sbi_video {
            display: block;
            position: relative;
        }
        #video-modal .modal__dialog {
          max-width: 1000px;
            width: 980px;
          padding: 1.2rem;
            margin-top: 150px;
        }
        .media-pop {
            cursor: pointer;
        }
        .sbi_playbtn {
            font-size: 30px !important;
            cursor: pointer;
        }
        .rmp-results-widget__avg-rating,
        .rmp-results-widget__visual-rating {
            display: none;
        }
        #vote-modal iframe {
            width: 100%;
            height: 100%;
            position: absolute;
            border: 0;
            display: none;
            overflow: hidden;
        }
        #vote-modal .modal-content {
            min-height: 530px;
        }
        .loading {
            position: absolute;
            top: 45%;
            left: 50%;
        }
        .ig-name {
            margin-top: 0;
            color: #333;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .ig-name p {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            height: 30px;            
        }
        .rmp-social-widget .rmp-icon--social {
            padding: 10px;
            font-size: 18px;
            width: 40px;
        }
        #sb_instagram #sbi_images .sbi_item.sbi_num_diff_hide {
            display: block !important;
        }

</style>
<section class="header-slider">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <div class="content-box">
                        <h1 class="header-title">Beerlife Chug Contest</h1>
                        <span class="header-subtitle">The chug with the most votes will win a <em>cash prize</em>.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <nav>
        <ul class="bl-nav-list">
            <li><a href="/entries" class="bl-nav-link">Instagram Video Entries</a></li>
            <li><a href="/leaderboard" class="bl-nav-link"><i class="fas fa-crown"></i> Leaderboards</a></li>
             <li><a href="/past-champion" class="bl-nav-link">Past Champions</a></li>
            <li>
                <a href="#">
                    <i class="fas fa-caret-down"></i>
                </a>
            </li>
        </ul>
        <ul class="bl-control-list">
            <?php if ( is_user_logged_in() )  { ?>
                <li id="loggedIn"><a href="/wp-login.php?action=logout" class="bl-nav-link logout" style="cursor: pointer;">Logout</a></li>
            <?php }else{ ?>
                <li id="loggedOut"><a href="/wp-login.php" class="bl-nav-link login" style="cursor: pointer;">Login</a></li>
            <?php } ?>
            <li><a href="" class="bl-nav-link submit">Submit a Video</a></li>
        </ul>
    </nav>
     <!--div class="filter-controls">
        <form id='search-form' class="search-filter">
            <div class="search-box">
                <input type="text" class="search-bar" placeholder="SEARCH FOR CHUG VIDEO ENTRIES" />
                <div class='search-btn'>
                    <button type='submit'>
                        <i class="fa fa-search search"></i>
                    </button>
                </div>
            </div>
        </form>
         <form action="" class="date-filter">
            <div class="select-box">
                <span>Chug Contest:</span>
                <select name="date" id="date">
                    <option value="May">May 2020</option>
                    <option value="June">June 2020</option>
                    <option value="July">July 2020</option>
                    <option value="August">August 2020</option>
                </select>
            </div>
        </form>
    </div --> 
    <section class="video-entries">

        <div id="sb_instagram" class="sbi  sbi_mob_col_auto sbi_col_5  sbi_width_resp sbi_medium" style="padding-bottom: 10px;width: 100%;height: 100%;">
            <div id="sbi_images" style="padding: 5px;">
                <?php            
                if (have_posts()) {
                    while ( have_posts() ) : the_post(); ?>

                        <?php 
                                $content = get_the_content();
                                $pid = get_the_ID();
                         ?>   
                        <div class="sbi_item sbi_type_video sbi_had_error" id="sbi_<?php echo $pid; ?>" data-date="0" data-numcomments="0">
                            <div class="sbi_photo_wrap">
                                        <svg style="color: rgba(255,255,255,1)" class="svg-inline--fa fa-play fa-w-14 sbi_playbtn" aria-label="Play" aria-hidden="true" data-fa-processed="" data-prefix="fa" data-icon="play" role="presentation" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M424.4 214.7L72.4 6.6C43.8-10.3 0 6.1 0 47.9V464c0 37.5 40.7 60.1 72.4 41.3l352-208c31.4-18.5 31.5-64.1 0-82.6z"></path></svg>      


                                <a class="sbi_photo media-pop" data-url="<?php echo $content; ?>"  rel="nofollow">
                                    <?php 
                                            $featured_img_url = get_the_post_thumbnail_url($pid,'full'); 
                                    ?>
                                     <img class="entry-thumb" src="<?php echo esc_url($featured_img_url) ?>" alt="<?php the_title() ?>" />
                                </a>
                                <div class="votes">
                                    <div>
                                        <span class="vote-counts">
                                             <?php echo do_shortcode("[ratemypost-result id='$pid']"); ?>
                                        </span>
                                        <span>VOTES</span>


                                    </div>
                                </div>
                            </div>
                            <div class="sbi_info">

                                
                                <h5 class="ig-name"><?php the_excerpt(); ?></h5>
                                <br>
                                
                                <button type="button" class="vote-btn text-center" data-pid="<?php echo $pid; ?>">Vote For This Chug </button>
                                <!--button type="button" class="share-btn">Share This Chug </button -->
                                <div class="entry-social-share">
                                    <?php echo do_shortcode("[ratemypost-result id='$pid' share='true']"); ?>
                                </div>
                                
                                 <!-- img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/social-media-icons-small.png" class="social-media-icons" alt="Social Media Icons" --> 
                            </div>
                        </div>

                <?php endwhile; ?>
            </div>   
        </div>
    <?php     
    // Reset Query
    wp_reset_query();
}
?>
    </section>
    <section class="faq-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/faq-icon.jpg" class="faq-icon" alt="Icon" />
                    <h2 class="section-title">Frequently asked question</h2>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Is there a limit to how many times I can participate?</h3>
                    <p class="faq-answer">Nope. There no limits how many times you can submit a new chug or drink. Simply drink, tag our account, and submit. We will donate $1 per submission.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Where exactly will the money be donated?</h3>
                    <p class="faq-answer">The money will be donated to DirectRelief.ord/emergency/coronavirus-outbrek/. This fund is dedicated to giving protective gear and critical care medications to as many health workers as possbile. Direct relief of the largest administrative charities in the space.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Why did we choose First Responders?</h3>
                    <p class="faq-answer">First Responders are currently fighting on the front lines in the battle against COVID-19. Central to the mission of Beerlife is the have fun and give back. We at Beerlife wanted to find a fun way to do good for those most in need during these challenging times.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">What is Beerlife Official</h3>
                    <p class="faq-answer">Beerlife is an irreverent entertainment and lifestyle brand for those who love beer. Our goal is to entertain our audience and make their lives more fun. We do that through our various social media pages on Instagram, Facebook, Pinterest, Beerlife.com, and original content.</p>
                </div>
            </div>
        </div>
    </section>
    <div id="login-modal">
        <div class="modal-body">
            <div class="modal-content">
                <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/beerlife-logo.png" class="modal-logo" alt="Site Logo"/>
                <h2 class="modal-title">Login to Beerlife Chug Contest</h2>
                <br>
                <span id="error_output" style="color: red; font-size: 13px; font-weight: 500;"></span>
                <form id="login_form">
                    <input type="text" placeholder="Email Address" name="email" class="modal-input">
                    <input type="password" placeholder="Password" name="password" class="modal-input">
                </form>
                
                <a href="#" class="forgot-password">Forgot Password?</a>
                <br>
                <button type="button" class="modal-btn btn-login" id="loginBtn">Login Now</button>
                <span class="modal-separator">OR</span>
                <button type="button" class="modal-btn btn-login-facebook"><i class="fab fa-facebook"></i> Continue With Facebook</button>
                <button type="button" class="modal-btn btn-login-google"><i class="fab fa-google"></i> Continue With Google</button>
                <span class="terms-agreement">By continuing, you agree to BeerLife’s<em> Terms of Service, Privacy Policy</em></span>

                <a class="sign-up">Not yet registered? Sign up</a>
                <!-- modal close button -->
                <a class="modal__close demo-close login-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>

    <div id="register-modal">
        <div class="modal-body">
            <div class="modal-content">
                <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/beerlife-logo.png" class="modal-logo" alt="Site Logo"/>
                <h2 class="modal-title">Create an Account</h2>
                <br>
                <form id="register_form">
                    <input type="text" placeholder="Email Address" name="r_email" id="r_email" class="modal-input">
                    <input type="password" placeholder="Password" name="r_password" id="r_password" class="modal-input">
                    <input type="number" placeholder="Age" name="age" class="modal-input">
                </form>
                <a href="#" class="forgot-password">Forgot Password?</a>
                <br>
                <button type="button" class="modal-btn btn-login" id="registerBtn">Sign Up Now</button>
                <span class="modal-separator">OR</span>
                <button type="button" class="modal-btn btn-login-facebook"><i class="fab fa-facebook"></i> Continue With Facebook</button>
                <button type="button" class="modal-btn btn-login-google"><i class="fab fa-google"></i> Continue With Google</button>
                <span class="terms-agreement">By continuing, you agree to BeerLife’s<em> Terms of Service, Privacy Policy</em></span>

                <a href="#" class="sign-up">Already a Member? Login</a>
                <!-- modal close button -->
                <a class="modal__close demo-close register-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>

    <div id="vote-modal" class="vote-modal">
        <div class="modal-body">
            <div class="modal-content">
                <iframe class="vote-entry" src="" scrolling="no"></iframe>
                <span class="loading"><i class="fas fa-3x fa-spinner fa-spin"></i></span>
                <!-- modal close button -->
                <a  class="modal__close demo-close vote-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>
    <div id="video-modal" class="video-modal">
        <div class="modal-body">
            <div class="modal-content">
                <div class="sbi_lb-container-wrapper">
                   <div class="sbi_lb-container" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                      <video class="sbi_video" src="" controls=""  style="opacity: 1; z-index: 1;"></video>
                   </div>
                </div>
                <!-- modal close button -->
                <a  class="modal__close demo-close video-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>


<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<?php get_footer(); ?>

<script>
jQuery(function($) {

    var ulLi = jQuery('nav ul.bl-nav-list > li'),
      fa = jQuery('nav ul.bl-nav-list > li:last-of-type a .fa');

    jQuery('nav ul.bl-nav-list').append('<ol></ol>');

    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/entries' class='bl-nav-link'>Instagram Video Entries</a></li>");
    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/leaderboard' class='bl-nav-link'><i class='fas fa-crown'></i> Leaderboards</a></li>");
    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/past-champion' class='bl-nav-link'>Past Champions</a></li>");

    jQuery('nav ul.bl-nav-list > li:last-of-type').on('click', function() {
        fa.toggleClass('fa-bars');
        fa.toggleClass('fa-times');
        jQuery(this).parent().children('ol').slideToggle(500);
    });

    $('.vote-btn').click(function(){
        SBI_ITEM = $(this).data('pid');
        var parent = '#sbi_' + $(this).data('pid'); 
        SBI_COUNT = parseInt($(parent).find('.js-rmp-vote-count').text());

        var current = $('#vote-modal .vote-entry').attr('src'),
            url = '/entry/?pid=' + $(this).data('pid');

        if(current == url) {
           $('#vote-modal').css('display', 'flex').fadeIn(500);
           $('#vote-modal .loading').hide();
           $('#vote-modal iframe').show();            
        }else{
            $('#vote-modal .vote-entry').attr('src', url);
             $('#vote-modal').css('display', 'flex').fadeIn(500);
             $('#vote-modal .loading').show();
             $('#vote-modal iframe').hide();

            setTimeout(function(){ 
                $('#vote-modal iframe').show();
               $('#vote-modal .loading').hide();
            }, 2000);            
        }
       
    });

    // $('.vote_close_btn').click(function(){
    //    $('#vote-modal').fadeOut(500);         
    //});


    $('.vote-modal-close').click(function(){
       // SBI_ITEM = $(this).data('pid');
        var parent = '#sbi_' + SBI_ITEM; 
        $(parent).find('.js-rmp-vote-count').text(SBI_COUNT);        
        $('#vote-modal').fadeOut(500);
    });

    /*
    $('.sbi_playbtn, .media-pop').click(function(){
        $parent = $(this).closest('.sbi_item');
        $parent.find('.vote-btn').click();
    });
    */


    
    $('.sbi_playbtn').click(function(){
        $parent = $(this).closest('.sbi_photo_wrap');
        $parent.find('.media-pop').click();
    });

    $('.media-pop').click(function(){
        var url = $(this).data('url');
        $('#video-modal .sbi_video').attr('src', url);
        $('#video-modal').fadeOut(500);
        $('#video-modal').css('display', 'flex').fadeIn(500);
    });


    $('.video-modal-close').click(function(){
        $('#video-modal').fadeOut(500);
    });

});
</script>