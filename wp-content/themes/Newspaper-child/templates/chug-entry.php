<?php
/**
 * Template Name: Clean Page
 * This template will only display the content you entered in the page editor
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<?php
$pid = $_GET['pid'];
$post_data = get_post( $pid  ); 

?>
<style type="text/css">
.vote-container ul{
    margin: 0;
    padding: 0;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    margin-bottom: 20px !important;    
}
.vote-container ul li{
    flex: 0 0 50%;
    list-style: none;
    margin-left: 0;
}
.vote-container ul li img{
    width: 100%;
    height: auto;
}
.vote-container ul li .ig-name{
    font-size: 30px;
    text-align: center;
    font-family: 'Karla', sans-serif;
}
.vote-container ul li .ig-name i{
    color: #b7142b;
}
.vote-container ul li .votes{
    width: 100%;
    text-align: center;
    margin-bottom: 15px;
}
.vote-container ul li .votes span{
    color: #b7142b;
    font-weight: 700;
}
.vote-container ul li .votes .vote-counts{
    font-size: 80px;
    line-height: 80px;
    display: block;
    margin-bottom: -5px;
}
.vote-container {
    padding: 10px;
}
.btn-vote {
    margin: auto;
    background-color: #B7142B;
    padding: 10px 20px;
    color: #fff;
    border-radius: 10px;
    cursor: pointer;
}
.confirm-vote.disabled {
    background-color: #cccccc;
    cursor: auto;
}
.video-title {
    margin-bottom: 40px;
    margin-top: -50px;
    color: #b7142b;
    height: 30px;
    overflow: hidden;
}
.video_content {
    position: relative;
    width: auto;
    height: 100%;
    max-width: 100%;
    max-height: 472px;
    margin: auto;
    display: block;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -ms-border-radius: 4px;
    -o-border-radius: 4px;
    border-radius: 4px;
}
.vote-details {
    padding: 5pxpx;
}
.rmp-widgets-container.rmp-wp-plugin.rmp-main-container .rmp-rating-widget__icons-list__icon {
    margin: auto;
    font-size: 20px;
}
.rmp-heading--subtitle {
    font-size: 18px;
}
.rmp-widgets-container.rmp-wp-plugin.rmp-main-container .rmp-rating-widget__submit-btn {
    display: none !important;
}
/*
.rmp-rating-widget__icons-list {
    display: none !important;
}
*/
.rmp-widgets-container.rmp-wp-plugin.rmp-main-container .rmp-social-widget {
    display: block;
    position: relative;
    bottom: -50px;    
}
.rmp-social-widget .rmp-icon--social {
    padding: 10px;
    font-size: 18px;
    width: 40px;
}
.rmp-rating-widget--has-rated .btn-vote {
    display: none !important;
}
</style>
<body class="cleanpage">
    <div class="container vote-container">
                <ul>
                    <li>
                        <video class="video_content" src="<?php echo $post_data->post_content; ?>" controls="" style="opacity: 1; z-index: 1;"></video>
                    </li>
                    <li>
                        <div class="text-center vote-details">
                            <h5 class="video-title"><?php echo $post_data->post_excerpt; ?></h5>
                             <?php echo do_shortcode("[ratemypost id='$pid']"); ?>
                        </div>
                    </li>
                </ul>
               
                <!-- modal close button -->
                <a  class="modal__close demo-close vote-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
    </div>
<script type="text/javascript">
 jQuery(function($){
    $('.btn-vote').click(function(){
        console.log('vote!');

        jQuery(this).html('<i class="fas fa-spinner fa-spin"></i>')
        setTimeout(function(){ jQuery('.js-submit-rating-btn').click(); }, 1000);

        $('body').on('DOMSubtreeModified', ".js-rmp-msg", function(el) {         
                $('.btn-vote').remove(); 
                    setTimeout(function(){ 
                        if( $('.rmp-rating-widget .rmp-rating-widget__msg--alert').length > 0 ) {
                            console.log('DONT COUNT!!!!!');
                            return false;
                        }else{
                            parent.SBI_COUNT = parent.SBI_COUNT + 1;
                            $('.vote-counts').text(parent.SBI_COUNT);              
                        }

                    }, 1500);
            ///location.reload();
        });  

    });  
 });
</script>
<?php wp_footer(); ?>
</body>
</html>
