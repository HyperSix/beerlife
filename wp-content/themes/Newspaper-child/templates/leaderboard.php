
<?php /* Template Name: Leaderboard */ 
?>

<?php session_start(); get_header(); ?>
<script type="text/javascript">
    var SBI_ITEM = '';
    var SBI_COUNT = 1;
</script>
<style type="text/css">
    
        .vote_close_btn {
            display: none;
            margin: auto;
            background-color: #B7142B;
            padding: 10px 20px;
            color: #fff;
            border-radius: 10px;
        }

        #login-modal {
            display: none;
        }
        #register-modal {
            display: none;
        }

        #video-modal {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            display: block;
            background: rgba(0,0,0,0.9);
            height: -webkit-fill-available;
            display: none;
            z-index: 9999;
            background: rgba(0,0,0,0.9);
            /* -webkit-box-align: start; */
            align-items: center;
            -webkit-box-pack: center;
        }
        #video-modal .modal-body {
            display: flex;
            position: relative;
            max-width: 480px;
            width: 100%;
            margin: auto;
            background: #ffffff;
        }
        #video-modal .modal-content {
            width: 100%;
            display: block;
            margin: auto;
            position: relative;
            background-color: #fff;
        }
        #video-modal .sbi_video {
            display: block;
            position: relative;
        }
        #video-modal .modal__dialog {
          max-width: 1000px;
            width: 980px;
          padding: 1.2rem;
            margin-top: 150px;
        }
        .media-pop {
            cursor: pointer;
        }
        .sbi_playbtn {
            font-size: 30px !important;
            cursor: pointer;
        }
        .rmp-results-widget__avg-rating,
        .rmp-results-widget__visual-rating {
            display: none;
        }
        #vote-modal iframe {
            width: 100%;
            height: 100%;
            position: absolute;
            border: 0;
            display: none;
            overflow: hidden;
        }
        #vote-modal .modal-content {
            min-height: 530px;
        }
        .loading {
            position: absolute;
            top: 45%;
            left: 50%;
        }
        .ig-name {
            margin-top: 0;
            color: #333;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .next-ten-videos .ig-name {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;      
            height: 26px;
            width: 100%;
            max-width: 250px;
            font-size: 16px;   
        }
        .next-ten-videos .vote-btn {
            font-size: 20px;
        }
        .rmp-social-widget .rmp-icon--social {
            padding: 10px;
            font-size: 18px;
            width: 40px;
        }
        #sb_instagram #sbi_images .sbi_item.sbi_num_diff_hide {
            display: block !important;
        }
        .votes {
            font-size: 20px;
        }
        .next-ten-videos .video-list li .list-box .top-content .votes span {
            font-size: 14px;
        }
        .other-videos .media-pop {
            display: inline-block;
            vertical-align: middle;
            margin-right: 15px;
        }
        .other-videos .media-pop img {
            height: 200px;
            width: 150px;
        }
        .other-videos .video-list li div .ig-name-box {
            width: 50%;
        }
        .other-videos .video-list li div .ig-name-box .ig-name {
            margin-top: 0;
            font-size: 15px;
            font-weight: normal;
            max-height: 200px;
            overflow: hidden;
        }
        .entry-thumb {
            max-height: 250px;
        }
        @media only screen and (max-width: 480px) {
            .main-content .top-video {
                display: none;
            }
        }
</style>
    <section class="header-slider">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <div class="content-box">
                        <h1 class="header-title">Beerlife Chug Contest</h1>
                        <span class="header-subtitle">The chug with the most votes will win a <em>cash prize</em>.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <nav>
        <ul class="bl-nav-list">
            <li><a href="/entries" class="bl-nav-link">Instagram Video Entries</a></li>
            <li><a href="/leaderboard" class="bl-nav-link"><i class="fas fa-crown"></i> Leaderboards</a></li>
             <li><a href="/past-champion" class="bl-nav-link">Past Champions</a></li> 
            <li>
                <a href="#">
                    <i class="fas fa-caret-down"></i>
                </a>
            </li>
        </ul>
        <ul class="bl-control-list">
            <?php if ( is_user_logged_in() )  { ?>
                <li id="loggedIn"><a href="/wp-login.php?action=logout" class="bl-nav-link logout" style="cursor: pointer;">Logout</a></li>
            <?php }else{ ?>
                <li id="loggedOut"><a href="/wp-login.php" class="bl-nav-link login" style="cursor: pointer;">Login</a></li>
            <?php } ?>
            <li><a href="" class="bl-nav-link submit">Submit a Video</a></li>
        </ul>
    </nav>
     <!--div class="filter-controls">
        <form id='search-form' class="search-filter">
            <div class="search-box">
                <input type="text" class="search-bar" placeholder="SEARCH FOR CHUG VIDEO ENTRIES" />
                <div class='search-btn'>
                    <button type='submit'>
                        <i class="fa fa-search search"></i>
                    </button>
                </div>
            </div>
        </form>
         <form action="" class="date-filter">
            <div class="select-box">
                <span>Chug Contest: </span>
                <select name="date" id="date">
                    <option value="May">May 2020</option>
                    <option value="June">June  2020</option>
                    <option value="July">July  2020</option>
                    <option value="August">August  2020</option>
                </select>
            </div>
        </form>
    </div --> 

        <?php 

            $top_post = rmp_get_top_rated_posts( 20, 1, 1 );
            $pid = $top_post[0]['postID'];
         ?>

    <section class="main-content">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <div class="content-box">
                        <h1 class="content-title">Top 10 Beerlife Chug Videos</h1>
                        <ul class="top-video"  id="sbi_<?php echo $pid; ?>" >
                            <li>
                                <video class="video_content" src="<?php echo $top_post[0]['video']; ?>" controls="" style="opacity: 1; z-index: 1;"></video>
                            </li>
                            <li>
                                 <h3 class="ig-name"><?php echo $top_post[0]['title']; ?></h3> 
                                <span class="vote-counts"><?php echo $top_post[0]['votes']; ?></span>
                                <span class="votes">VOTES</span>
                                <br>
                                 <button type="button" class="vote-btn text-center" data-pid="<?php echo $pid; ?>">Vote For This Chug </button> 
                                <?php echo do_shortcode("[ratemypost-result id='$pid' share='true']"); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="next-ten-videos">
        <ul class="video-list">
            <?php 
                $ctr = 0;
                foreach ($top_post as $entry) : 
                    $pid = $entry['postID'];
                    $ctr++;
                    if($ctr > 10) break;
            ?>

            <li id="sbi_<?php echo $pid; ?>">
                <div class="list-box">
                    <div class="top-content">
                        <div class="video-rank"><span><?php echo $ctr; ?></span></div>
                        <a class="sbi_photo media-pop" data-url="<?php echo $entry['video']; ?>"  rel="nofollow">
                             <img class="entry-thumb" src="<?php echo $entry['thumb']; ?>" alt="" />
                        </a>                        
                        <div class="votes">
                            <div>
                                <span class="vote-counts">
                                    <?php echo do_shortcode("[ratemypost-result id='$pid']"); ?>
                                </span>
                                <span>VOTES</span>
                            </div>
                        </div>
                    </div>
                    
                     <h3 class="ig-name"><?php echo $entry['title']; ?></h3> 
                    <br>
                     <button type="button" class="vote-btn text-center" data-pid="<?php echo $pid; ?>">Vote For This Chug </button> 
                    <?php echo do_shortcode("[ratemypost-result id='$pid' share='true']"); ?>
                </div>
            </li>
           <?php endforeach; ?>
        </ul>
    </section>
    <section class="other-videos">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <ul class="video-list">
                        <?php 
                            $ctr = 0;
                            foreach ($top_post as $entry) : 
                                $ctr++;
                                //if($ctr <= 10) continue;

                                //if($ctr > 20) break;
                                if($ctr > 10) break;

                                $pid = $entry['postID'];
                        ?>                        
                        <li  id="sbi_<?php echo $pid; ?>">
                            <div class="first-column">
                                <div class="rank-circle">
                                    <span><?php echo $ctr; ?></span>
                                </div>
                            </div>
                            <div class="second-column">
                                    <a class="sbi_photo media-pop" data-url="<?php echo $entry['video']; ?>"  rel="nofollow">
                                        <img class="entry-thumb" src="<?php echo $entry['thumb']; ?>" alt="" />
                                    </a>
                                <div class="ig-name-box">
                                     <h3 class="ig-name"><?php echo substr($entry['title'], 0, 400) ."..."; ?></h3> 
                                    <div class="votes-mobile">
                                        <span class="vote-counts">
                                            <?php echo do_shortcode("[ratemypost-result id='$pid']"); ?>
                                        </span>
                                        <span class="vote">VOTES</span>
                                    </div>
                                </div>
                            </div>
                            <div class="third-column">
                                <span class="vote-counts">
                                    <?php echo do_shortcode("[ratemypost-result id='$pid']"); ?>
                                </span>
                                <span class="vote">VOTES</span>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="faq-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/faq-icon.jpg" class="faq-icon" alt="Icon" />
                    <h2 class="section-title">Frequently asked question</h2>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Is there a limit to how many times I can participate?</h3>
                    <p class="faq-answer">Nope. There no limits how many times you can submit a new chug or drink. Simply drink, tag our account, and submit. We will donate $1 per submission.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Where exactly will the money be donated?</h3>
                    <p class="faq-answer">The money will be donated to DirectRelief.ord/emergency/coronavirus-outbrek/. This fund is dedicated to giving protective gear and critical care medications to as many health workers as possbile. Direct relief of the largest administrative charities in the space.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Why did we choose First Responders?</h3>
                    <p class="faq-answer">First Responders are currently fighting on the front lines in the battle against COVID-19. Central to the mission of Beerlife is the have fun and give back. We at Beerlife wanted to find a fun way to do good for those most in need during these challenging times.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">What is Beerlife Official</h3>
                    <p class="faq-answer">Beerlife is an irreverent entertainment and lifestyle brand for those who love beer. Our goal is to entertain our audience and make their lives more fun. We do that through our various social media pages on Instagram, Facebook, Pinterest, Beerlife.com, and original content.</p>
                </div>
            </div>
        </div>
    </section>

    <div id="vote-modal" class="vote-modal">
        <div class="modal-body">
            <div class="modal-content">
                <iframe class="vote-entry" src="" scrolling="no"></iframe>
                <span class="loading"><i class="fas fa-3x fa-spinner fa-spin"></i></span>
                <!-- modal close button -->
                <a  class="modal__close demo-close vote-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>
    <div id="video-modal" class="video-modal">
        <div class="modal-body">
            <div class="modal-content">
                <div class="sbi_lb-container-wrapper">
                   <div class="sbi_lb-container" style="touch-action: pan-y; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                      <video class="sbi_video" src="" controls=""  style="opacity: 1; z-index: 1;"></video>
                   </div>
                </div>
                <!-- modal close button -->
                <a  class="modal__close demo-close video-modal-close">
                    <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                </a>
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<?php get_footer(); ?>
<script>
jQuery(function($) {
     console.log('LEADERBOARD....');
    
    var ulLi = jQuery('nav ul.bl-nav-list > li'),
      fa = jQuery('nav ul.bl-nav-list > li:last-of-type a .fa');

    jQuery('nav ul.bl-nav-list').append('<ol></ol>');

    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/entries' class='bl-nav-link'>Instagram Video Entries</a></li>");
    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/leaderboard' class='bl-nav-link'><i class='fas fa-crown'></i> Leaderboards</a></li>");
    jQuery('nav ul.bl-nav-list > ol').append("<li><a href='/past-champion' class='bl-nav-link'>Past Champions</a></li>");

    jQuery('nav ul.bl-nav-list > li:last-of-type').on('click', function() {
        fa.toggleClass('fa-bars');
        fa.toggleClass('fa-times');
        jQuery(this).parent().children('ol').slideToggle(500);
    });

     $('.vote-btn').click(function(){
        SBI_ITEM = $(this).data('pid');
        var parent = '#sbi_' + $(this).data('pid'); 
        SBI_COUNT = parseInt($(parent).find('.js-rmp-vote-count').text());

        var current = $('#vote-modal .vote-entry').attr('src'),
            url = '/entry/?pid=' + $(this).data('pid');

        if(current == url) {
           $('#vote-modal').css('display', 'flex').fadeIn(500);
           $('#vote-modal .loading').hide();
           $('#vote-modal iframe').show();            
        }else{
            $('#vote-modal .vote-entry').attr('src', url);
             $('#vote-modal').css('display', 'flex').fadeIn(500);
             $('#vote-modal .loading').show();
             $('#vote-modal iframe').hide();

            setTimeout(function(){ 
                $('#vote-modal iframe').show();
               $('#vote-modal .loading').hide();
            }, 2000);            
        }
       
    });


    $('.vote-modal-close').click(function(){
       // SBI_ITEM = $(this).data('pid');
        var parent = '#sbi_' + SBI_ITEM; 
        $(parent).find('.js-rmp-vote-count').text(SBI_COUNT);        
        $('#vote-modal').fadeOut(500);
    });

    /*
    $('.sbi_playbtn, .media-pop').click(function(){
        $parent = $(this).closest('.sbi_item');
        $parent.find('.vote-btn').click();
    });
    */
    
    $('.sbi_playbtn').click(function(){
        $parent = $(this).closest('.sbi_photo_wrap');
        $parent.find('.media-pop').click();
    });

    $('.media-pop').click(function(){
        var url = $(this).data('url');
        $('#video-modal .sbi_video').attr('src', url);
        $('#video-modal').fadeOut(500);
        $('#video-modal').css('display', 'flex').fadeIn(500);
    });


    $('.video-modal-close').click(function(){
        $('#video-modal').fadeOut(500);
    });

});
</script>