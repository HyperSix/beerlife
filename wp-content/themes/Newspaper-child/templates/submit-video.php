<?php /* Template Name: Submit Video */ ?>
<?php get_header(); ?>
    <section class="header-slider">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <div class="content-box">
                        <h1 class="header-title">Beerlife Chug Contest</h1>
                        <span class="header-subtitle">The chug with the most votes will win a <em>cash prize</em>.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <nav>
        <ul class="bl-nav-list">
            <li><a href="https://beerlife.com/entries" class="bl-nav-link">Instagram Video Entries</a></li>
            <li><a href="https://beerlife.com/leaderboard" class="bl-nav-link"><i class="fas fa-crown"></i> Leaderboards</a></li>
            <li><a href="https://beerlife.com/past-champion" class="bl-nav-link">Past Champions</a></li>
            <li>
                <a href="#">
                    <i class="fas fa-caret-down"></i>
                </a>
            </li>
        </ul>
        <ul class="bl-control-list">
            <li><a href="#" data-modal="#modal2" class="bl-nav-link login modal__trigger">Login</a></li>
            <li><a href="https://beerlife.com/submit-video" class="bl-nav-link submit">Submit a Video</a></li>
        </ul>
    </nav>
    <section class="main-content">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <h1 class="content-title">Submit a Chug Video Entry</h1>
                    <br>
                    <div class="submit-box">
                        <span class="box-title">Upload Your Video</span>
                        <div class="dropzone">
                            <div id="dropZ">
                                <h2 class="instruction">Drag and Drop Video Here</h2>                    
                                <span>or</span>
                                <div class="selectFile">       
                                <label for="file">Select file</label>                   
                                <input type="file" name="files[]" id="file">
                                <span id="file-name"></span>
                                </div>
                                <p>Maximum file size is 20mb only.</p>
                            </div>
                        </div>
                        <br>
                        <div class="url-box">
                            <span class="box-title">Upload Your Video</span>
                            <input type="text" name="url" value="" class="url">
                        </div>
                        
                        <input type="checkbox" id="read" name="read" value="read">
                        <label class="terms" for="read"> I have read the <em>terms of submission</em></label>
                        <br>
                        <button type="submit" class="submit-video" name="submit">Submit Video Now</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="faq-section">
        <div class="bl-container td-stretch-content">
            <div class="bl-row">
                <div class="bl-col-md-12 text-center">
                    <img src="https://beerlife.com/wp-content/themes/Newspaper/challenge-images/faq-icon.jpg" class="faq-icon" alt="Icon" />
                    <h2 class="section-title">Frequently asked question</h2>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Is there a limit to how many times I can participate?</h3>
                    <p class="faq-answer">Nope. There no limits how many times you can submit a new chug or drink. Simply drink, tag our account, and submit. We will donate $1 per submission.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Where exactly will the money be donated?</h3>
                    <p class="faq-answer">The money will be donated to DirectRelief.ord/emergency/coronavirus-outbrek/. This fund is dedicated to giving protective gear and critical care medications to as many health workers as possbile. Direct relief of the largest administrative charities in the space.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">Why did we choose First Responders?</h3>
                    <p class="faq-answer">First Responders are currently fighting on the front lines in the battle against COVID-19. Central to the mission of Beerlife is the have fun and give back. We at Beerlife wanted to find a fun way to do good for those most in need during these challenging times.</p>
                </div>
                <div class="bl-col-md-6">
                    <h3 class="faq-question">What is Beerlife Official</h3>
                    <p class="faq-answer">Beerlife is an irreverent entertainment and lifestyle brand for those who love beer. Our goal is to entertain our audience and make their lives more fun. We do that through our various social media pages on Instagram, Facebook, Pinterest, Beerlife.com, and original content.</p>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
<script>
jQuery(document).bind('dragover', function (e) {
    var dropZone = jQuery('.dropzone'),
        timeout = window.dropZoneTimeout;
    if (!timeout) {
        dropZone.addClass('in');
    } else {
        clearTimeout(timeout);
    }
    var found = false,
        node = e.target;
    do {
        if (node === dropZone[0]) {
            found = true;
            break;
        }
        node = node.parentNode;
    } while (node != null);
    if (found) {
        dropZone.addClass('hover');
    } else {
        dropZone.removeClass('hover');
    }
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
});
</script>
<script>
    jQuery(document).ready(function(){
        jQuery('#file').change(function(e){
            var fileName = e.target.files[0].name;
            document.getElementById("file-name").value = "fileName";
        });
    });
</script>