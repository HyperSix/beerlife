<?php
/*
This script creates WP post from IG 
*/

require( 'wp-load.php' );

echo "processing... \n";

//$table_name = $wpdb->prefix . SBI_INSTAGRAM_POSTS_TYPE;

$prefix = $wpdb->prefix;
$img_path = '/wp-content/uploads/sb-instagram-feed-images/';
$category = 2592;
// Get the path to the upload directory.
$wp_upload_dir = wp_upload_dir();

//get data
$query = "SELECT * FROM ". $prefix ."sbi_instagram_posts WHERE `posted` = 0";

$results = $wpdb->get_results($query);

foreach ($results as $result) {
    
    $id  		= $result->id;
    $img_name	= $result->media_id .'full.jpg';
    $data 		= json_decode($result->json_data);
    $ig_link 	= urldecode($data->permalink);
    $caption 	= $data->caption;
    $vid_link	= $data->media_url;
    //$wpdb->query($sql);

	// Create post object
	$my_post = array(
	  'post_title'    => 'Chug Video Entry',
	  'post_content'  => $vid_link,
	  'post_status'   => 'publish',
	  'post_author'   => 1,
	  'post_type'	  => 'chug-entry',
	  'post_category' => $category,
	  'post_excerpt'  => $caption
	);
	 
	// Insert the post into the database
	$parent_post_id = wp_insert_post( $my_post );

	add_post_meta( $parent_post_id, 'chug_ig_url', $ig_link, true );

	// $filename should be the path to a file in the upload directory.
	$filename = $img_path . $img_name;
	 
	// Check the type of file. We'll use this as the 'post_mime_type'.
	$filetype = wp_check_filetype( basename( $filename ), null );
	 
	// Prepare an array of post data for the attachment.
	$attachment = array(
	    //'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
	    'guid'           => $filename, 
	    'post_mime_type' => $filetype['type'],
	    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
	    'post_content'   => $filename,
	    'post_status'    => 'inherit'
	);
	 
	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );
	 
	// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	 
	// Generate the metadata for the attachment, and update the database record.
	$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
	wp_update_attachment_metadata( $attach_id, $attach_data );
	 
	set_post_thumbnail( $parent_post_id, $attach_id );

	$query = "UPDATE ". $prefix ."sbi_instagram_posts SET posted = 1 WHERE id  = $id ";
	$wpdb->query($query);
}
echo "DONE!";